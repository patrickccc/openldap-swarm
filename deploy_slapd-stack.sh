#!/bin/bash
#set -x

# v:10.1 9-sept-2021

# Setup environment
export $(cat openldap.env) > /dev/null 2>&1

# Setup Docker Swarm Mode
if [ "$(docker info | grep Swarm | sed 's/ Swarm: //g')" == "inactive" ]; then
  docker swarm init --advertise-addr $IPADDR
fi

if [ ! -L letsencrypt ];then
    ln -s $letsencrypt_src letsencrypt
fi

target=$(docker network ls |grep slapd-shared)
if [ ! "$target" ]; then
  echo
  echo "slapd-shared Network not present ... creating"
  echo
  docker network prune -f
  docker network create --driver overlay --attachable slapd-shared
fi

if [ ! -d "$DATA_STORE" ]; then
  mkdir -p $DATA_STORE/database
  mkdir -p $DATA_STORE/config
  mkdir -p $DATA_STORE/openldap-certs
  mkdir -p $DATA_STORE/phpadmin-certs
  mkdir -p $DATA_STORE/phpadmin-env
  mkdir -p $DATA_STORE/bootstrap/ldif

  cp -ur ./phpadmin-env/* $DATA_STORE/phpadmin-env/

  # only copy bootstrap ldif files the first time.
  # Use the extend_schema.sh script to update schema.
  cp ./src/buildOU.ldif $DATA_STORE/bootstrap/ldif
fi

# Always update certs because they expire
#dumpcerts.acme.v2.sh ./letsencrypt/acme.json ./letsencrypt
cp -u ./letsencrypt/certs/$LDAP_HOST.crt $DATA_STORE/openldap-certs/ldap.crt
cp -u ./letsencrypt/certs/$LDAP_HOST.crt $DATA_STORE/openldap-certs/ca.crt
cp -u ./letsencrypt/private/$LDAP_HOST.key $DATA_STORE/openldap-certs/ldap.key

cp -u ./letsencrypt/certs/$LDAP_HOST.crt $DATA_STORE/phpadmin-certs/phpldapadmin.crt
cp -u ./letsencrypt/certs/$LDAP_HOST.crt $DATA_STORE/phpadmin-certs/ca.crt
cp -u ./letsencrypt/private/$LDAP_HOST.key $DATA_STORE/phpadmin-certs/phpldapadmin.key

# Secrets should not change once deployed.
other_secrets=$(docker secret ls | grep ldap_admin_password)
if [ ! "$(docker secret ls | grep ldap_admin_password)" ];then
  if [ -s ./src/secrets/ldap_admin_password ] && [ -s ./src/secrets/readonly_user_password ];then
    docker secret create ldap_admin_password ./src/secrets/ldap_admin_password
    docker secret create readonly_user_password ./src/secrets/readonly_user_password
  else
    echo
    echo "   NO suitable secrets available"
    docker secret ls
  fi
fi

if [ "$(docker secret ls | grep ldap_admin_password)" ];then
  docker stack deploy -c slapd-phpadmin-compose.yml slapd

  echo 'docker service logs -f slapd_openldap'
  echo 'docker service logs -f slapd_phpadmin'
fi
