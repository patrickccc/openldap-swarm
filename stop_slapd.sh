#!/bin/bash
#set -x

docker stack rm slapd
sleep 5
if [ $(docker container ls -a --filter 'exited=0' --format "{{.Names}}" | grep -i slapd) ]; then
  docker rm $t1
fi
docker secret rm ldap_admin_password readonly_user_password
