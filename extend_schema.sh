#!/bin/bash
#set -x

# following won't run if docker was started with an attached interactive shell.
# updates to add necessary eduPerson & voPerson objectClasses to the DIT.
# voPosixAccount & eduMember.ldif may become necessary later.

export $(cat openldap.env) > /dev/null 2>&1
cp ./src/schema/eduPerson.ldif $DATA_STORE/config/cn\=config/cn\=schema
cp ./src/schema/voPerson.ldif $DATA_STORE/config/cn\=config/cn\=schema
cp ./src/schema/voPosixAccount.ldif $DATA_STORE/config/cn\=config/cn\=schema
#cp ./src/schema/eduMember.ldif $DATA_STORE/config/cn\=config/cn\=schema

docker exec $(docker ps -q -f name=slapd_openldap) chown openldap:openldap /etc/ldap/slapd.d/cn\=config/cn\=schema/eduPerson.ldif
docker exec $(docker ps -q -f name=slapd_openldap) chown openldap:openldap /etc/ldap/slapd.d/cn\=config/cn\=schema/voPerson.ldif
docker exec $(docker ps -q -f name=slapd_openldap) chown openldap:openldap /etc/ldap/slapd.d/cn\=config/cn\=schema/voPosixAccount.ldif
#docker exec $(docker ps -q -f name=slapd_openldap) chown openldap:openldap /etc/ldap/slapd.d/cn\=config/cn\=schema/eduMember.ldif

docker exec $(docker ps -q -f name=slapd_openldap) ls -l /etc/ldap/slapd.d/cn\=config/cn\=schema
docker exec $(docker ps -q -f name=slapd_openldap) slaptest
chmod 600 $DATA_STORE/config/cn\=config/cn\=schema/*.ldif

# update running service
docker service update slapd_openldap

# remove stopped services
# docker container ls -a --filter 'exited=0' --format 'table {{.ID}}\t{{.Names}}' | grep -i slapd_openldap
sleep 15
t1=$(docker container ls -a --filter 'exited=0' --format "{{.Names}}" | grep -i slapd)
if [ -n $t1 ]; then
  docker rm $t1
fi


#docker exec $(docker ps -q -f name=slapd_openldap) ldapsearch -s sub -H ldaps://aai-demo.bio.ausfed.net -b dc=bio,dc=ausfed,dc=net -D "cn=admin,dc=bio,dc=ausfed,dc=net" -w <passwd>
#docker ps -a --filter 'exited=0' --format "{{json .Names}}" | grep slapd
